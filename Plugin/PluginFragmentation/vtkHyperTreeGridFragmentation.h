/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkHyperTreeGridFragmentation.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class   vtkHyperTreeGridFragmentation
 * @brief   Extract fragments from HyperTreeGrid
 *
 *
 * Extract fragment from HyperTreeGrid. Then compute the velocity and the mass of fragments.
 *
 * @sa
 * vtkHyperTreeGrid vtkHyperTreeGridAlgorithm
 *
 * @par Thanks:
 * This work was supported by Commissariat a l'Energie Atomique
 * CEA, DAM, DIF, F-91297 Arpajon, France.
 */

#ifndef VTK_HYPER_TREE_GRID_FRAGMENTATION_H
#define VTK_HYPER_TREE_GRID_FRAGMENTATION_H

#include <list>
#include <map>
#include <set>
#include <vector>

#include "vtkDoubleArray.h"
#include "vtkHyperTreeGridAlgorithm.h"
#include "vtkHyperTreeGridNonOrientedCursor.h"
#include "vtkHyperTreeGridNonOrientedGeometryCursor.h"
#include "vtkHyperTreeGridNonOrientedVonNeumannSuperCursor.h"
#include "vtkIntArray.h"
#include "vtkPoints.h"
//--------------------------------------------------------------------------------------------------------------------

class vtkHyperTreeGridFragmentation : public vtkHyperTreeGridAlgorithm
{
public:
  vtkTypeMacro(vtkHyperTreeGridFragmentation, vtkHyperTreeGridAlgorithm);
  virtual void PrintSelf(ostream& os, vtkIndent indent);

  static vtkHyperTreeGridFragmentation* New();

  vtkSetStringMacro(MassName);
  vtkGetStringMacro(MassName);

  vtkSetStringMacro(DensityName);
  vtkGetStringMacro(DensityName);

  vtkSetStringMacro(VelocityName);
  vtkGetStringMacro(VelocityName);

  vtkSetMacro(ExtractEdge, bool);
  vtkGetMacro(ExtractEdge, bool);

  vtkSetMacro(EnableMassName, bool);
  vtkGetMacro(EnableMassName, bool);

  vtkSetMacro(EnableDensityName, bool);
  vtkGetMacro(EnableDensityName, bool);

  vtkSetMacro(EnableVelocityName, bool);
  vtkGetMacro(EnableVelocityName, bool);

  virtual int FillOutputPortInformation(int, vtkInformation*) override;
  virtual int ProcessTrees(vtkHyperTreeGrid*, vtkDataObject*) override;

protected:
  vtkHyperTreeGridFragmentation() = default;
  virtual ~vtkHyperTreeGridFragmentation() override;

  char* MassName = nullptr;
  char* DensityName = nullptr;
  char* VelocityName = nullptr;

  void RecursivelyProcessTree(vtkHyperTreeGridNonOrientedVonNeumannSuperCursor*);
  void GenPoints(vtkHyperTreeGridNonOrientedGeometryCursor*);

  vtkBitArray* InMask;
  int m_number_of_children;
  int m_dimension;

  bool ExtractEdge = false;
  bool EnableMassName = false;
  bool EnableDensityName = false;
  bool EnableVelocityName = false;

  // les outputs
  std::map<int, int> BlockId;
  std::vector<vtkPoints*> Points;
  vtkIntArray* GhostArray;

  // Les datas
  vtkDoubleArray* MassArray = nullptr;
  vtkDoubleArray* DensityArray = nullptr;
  vtkDoubleArray* VelocityArray = nullptr;

  // les fragments
  std::vector<std::list<vtkIdType> > Fragments;
  std::vector<int> FragIds;

  // Les bords de fragments
  std::vector<bool> Bords;

  // Les caracteristiques par fragment
  std::vector<long> m_pounds;
  std::vector<double> m_volume;
  std::vector<double> m_mass;
  std::vector<double*> m_avg_velocity; // average velocity
  std::vector<double*> m_velocity;     // weight velocity against mass

private:
  vtkHyperTreeGridFragmentation(const vtkHyperTreeGridFragmentation&) = delete;
  void operator=(const vtkHyperTreeGridFragmentation&) = delete;
};

#endif // VTK_HYPER_TREE_GRID_FRAGMENTATION_H
