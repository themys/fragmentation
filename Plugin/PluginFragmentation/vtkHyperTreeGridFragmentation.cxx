/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkHyperTreeGridFragmentation.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include <math.h>

#include "vtkHyperTreeGrid.h"
#include "vtkHyperTreeGridFragmentation.h"

#include "vtkHyperTreeGridNonOrientedCursor.h"
#include "vtkHyperTreeGridNonOrientedVonNeumannSuperCursor.h"

#include "vtkBitArray.h"
#include "vtkCellData.h"
#include "vtkDataObject.h"
#include "vtkDoubleArray.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"
#include "vtkStreamingDemandDrivenPipeline.h"

#include "vtkMPIController.h"
#include "vtkMultiProcessController.h"
//-------------------------------------------------------------------------------------------------
vtkStandardNewMacro(vtkHyperTreeGridFragmentation);
//--------------------------------------------------------------------------------------------------
vtkHyperTreeGridFragmentation::~vtkHyperTreeGridFragmentation()
{
  if (this->MassName)
  {
    delete[] this->MassName;
  }
  if (this->DensityName)
  {
    delete[] this->DensityName;
  }
  if (this->VelocityName)
  {
    delete[] this->VelocityName;
  }
}
//--------------------------------------------------------------------------------------------------
int identity(int i)
{
  return i;
}
//--------------------------------------------------------------------------------------------------
int vtkHyperTreeGridFragmentation::ProcessTrees(vtkHyperTreeGrid* input, vtkDataObject* outputDO)
{
  this->InMask = input->HasMask() ? input->GetMask() : nullptr;
  this->m_number_of_children = input->GetNumberOfChildren();
  this->m_dimension = input->GetDimension();

  this->FragIds.clear();
  this->Fragments.clear();
  this->FragIds.resize(input->GetNumberOfVertices(), -1);
  this->Bords.resize(input->GetNumberOfVertices(), false);
  this->m_pounds.clear();
  this->m_volume.clear();
  this->m_mass.clear();
  for (auto it = this->m_avg_velocity.begin(); it != this->m_avg_velocity.end(); ++it)
  {
    if (*it)
    {
      delete[] * it;
    }
  }
  this->m_avg_velocity.clear();
  for (auto it = this->m_velocity.begin(); it != this->m_velocity.end(); ++it)
  {
    if (*it)
    {
      delete[] * it;
    }
  }
  this->m_velocity.clear();

  vtkMultiBlockDataSet* output = vtkMultiBlockDataSet::SafeDownCast(outputDO);

  this->GhostArray = vtkIntArray::SafeDownCast(
    input->GetCellData()->GetArray(vtkDataSetAttributes::GhostArrayName()));

  this->MassArray = nullptr;
  if (this->EnableMassName && this->MassName != "")
  {
    this->MassArray = vtkDoubleArray::SafeDownCast(input->GetCellData()->GetArray(MassName));
    // TODO JB Traiter aussi le cas vtkFloatArray !
  }

  this->DensityArray = nullptr;
  if (this->EnableDensityName && this->DensityName != "")
  {
    this->DensityArray = vtkDoubleArray::SafeDownCast(input->GetCellData()->GetArray(DensityName));
    // TODO JB Traiter aussi le cas vtkFloatArray !
  }

  this->VelocityArray = nullptr;
  if (this->EnableVelocityName && this->VelocityName != "")
  {
    vtkDoubleArray* arr =
      vtkDoubleArray::SafeDownCast(input->GetCellData()->GetArray(VelocityName));
    if (arr && arr->GetNumberOfComponents() == 3)
    {
      this->VelocityArray = arr;
    }
    // TODO JB Traiter aussi le cas vtkFloatArray !
  }

  vtkIdType index;
  vtkHyperTreeGrid::vtkHyperTreeGridIterator it;
  input->InitializeTreeIterator(it);
  vtkNew<vtkHyperTreeGridNonOrientedVonNeumannSuperCursor> cursor;
  while (it.GetNextTree(index))
  {
    input->InitializeNonOrientedVonNeumannSuperCursor(cursor, index);
    this->RecursivelyProcessTree(cursor);
  }

  for (int i = 0; i < this->m_avg_velocity.size(); ++i)
    for (int k = 0; k < 3; ++k)
      if (this->m_pounds[i] == 0)
      {
        assert(!this->m_avg_velocity.at(i));
      }
      else
      {
        this->m_avg_velocity[i][k] /= this->m_pounds[i];
      }

  for (int i = 0; i < this->m_velocity.size(); ++i)
    for (int k = 0; k < 3; ++k)
      if (this->m_mass[i] == 0)
      {
        assert(!this->m_avg_velocity.at(i));
      }
      else
      {
        this->m_velocity[i][k] /= this->m_mass[i];
      }

  int nbFrags = 0;
  for (int i = 0; i < this->FragIds.size(); ++i)
  {
    if (this->FragIds[i] >= 0 && this->BlockId.find(this->FragIds[i]) == this->BlockId.end())
    {
      this->BlockId[this->FragIds[i]] = nbFrags++;
    }
  }

  for (int i = 0; i < nbFrags; ++i)
  {
    this->Points.push_back(vtkPoints::New());
  }

  input->InitializeTreeIterator(it);
  vtkNew<vtkHyperTreeGridNonOrientedGeometryCursor> cur_pt;
  while (it.GetNextTree(index))
  {
    input->InitializeNonOrientedGeometryCursor(cur_pt, index);
    this->GenPoints(cur_pt);
  }

  vtkPoints* pts_ctr = vtkPoints::New();
  pts_ctr->SetNumberOfPoints(nbFrags);

  output->SetNumberOfBlocks(nbFrags + 1);
  std::vector<int> IdgFrag_to_IndexFrag(nbFrags);
  for (auto it = this->BlockId.begin(); it != this->BlockId.end(); ++it)
  {
    int index_frag = it->first; // gap numbering
    int idg_frag = it->second;  // continuous 0 to ...

    IdgFrag_to_IndexFrag[idg_frag] = index_frag;

    double center[3] = { 0, 0, 0 };
    vtkPolyData* pd = vtkPolyData::New();
    pd->SetPoints(this->Points[idg_frag]);

    vtkPoints* pt_ptr = this->Points[idg_frag];
    vtkIdType np = pt_ptr->GetNumberOfPoints();
    vtkCellArray* vertices = vtkCellArray::New();
    vertices->AllocateEstimate(np, 1);
    for (vtkIdType i = 0; i < np; ++i)
    {
      vertices->InsertNextCell(1, &i);
      double* tmp = pt_ptr->GetPoint(i);
      for (int k = 0; k < 3; ++k)
        center[k] += tmp[k];
    } // i
    pd->SetVerts(vertices);
    for (int k = 0; k < 3; ++k)
      center[k] /= np;
    pts_ctr->SetPoint(idg_frag, center);

    this->Points[idg_frag]->Delete();

#define SET_FIELD(type, name, nbcompo, action)                                                     \
  {                                                                                                \
    type* data = type::New();                                                                      \
    data->SetName(name);                                                                           \
    data->SetNumberOfComponents(nbcompo);                                                          \
    action;                                                                                        \
    pd->GetFieldData()->AddArray(data);                                                            \
    data->Delete();                                                                                \
  }

#define SET_FIELD_SCALAR(type, name, value) SET_FIELD(type, name, 1, data->InsertNextValue(value))

#define SET_FIELD_VECTOR(type, name, value) SET_FIELD(type, name, 3, data->InsertNextTuple(value))

    SET_FIELD_SCALAR(vtkIntArray, "FragmentId", idg_frag)
    SET_FIELD_SCALAR(vtkIntArray, "Pounds", this->m_pounds[index_frag])
    SET_FIELD_SCALAR(vtkDoubleArray, "Volume", this->m_volume[index_frag])

    if (this->DensityArray || this->MassArray)
    {
      SET_FIELD_SCALAR(vtkDoubleArray, "Mass", this->m_mass[index_frag])
      SET_FIELD_SCALAR(
        vtkDoubleArray, "Density", this->m_mass[index_frag] / this->m_volume[index_frag])
    }

    if (this->VelocityArray)
    {
      SET_FIELD_VECTOR(vtkDoubleArray, "AvgVelocity", this->m_avg_velocity[index_frag])
      if (this->DensityArray || this->MassArray)
      {
        SET_FIELD_VECTOR(vtkDoubleArray, "Velocity", this->m_velocity[index_frag])
      }
    }

    vertices->Delete();

    output->SetBlock(idg_frag + 1, pd);
    char name[32];
    sprintf(name, "Fragment_%d", idg_frag);
    output->GetMetaData(idg_frag + 1)->Set(vtkCompositeDataSet::NAME(), name);
  }

  vtkPolyData* center = vtkPolyData::New();
  center->SetPoints(pts_ctr);
  pts_ctr->Delete();

  vtkCellArray* vertices_ctr = vtkCellArray::New();
  vertices_ctr->AllocateEstimate(nbFrags, 1);
  for (vtkIdType i = 0; i < nbFrags; ++i)
  {
    vertices_ctr->InsertNextCell(1, &i);
  } // i
  center->SetVerts(vertices_ctr);
  vertices_ctr->Delete();

#define SET_POINT(type, name, nbcompo, action)                                                     \
  {                                                                                                \
    type* data = type::New();                                                                      \
    data->SetName(name);                                                                           \
    data->SetNumberOfComponents(nbcompo);                                                          \
    for (int i = 0; i < nbFrags; ++i)                                                              \
    {                                                                                              \
      action;                                                                                      \
    }                                                                                              \
    center->GetPointData()->AddArray(data);                                                        \
    data->Delete();                                                                                \
  }

#define SET_POINT_SCALAR(type, name, action) SET_POINT(type, name, 1, data->InsertNextValue(action))

#define SET_POINT_VECTOR(type, name, action) SET_POINT(type, name, 3, data->InsertNextTuple(action))

  SET_POINT_SCALAR(vtkIntArray, "FragmentId", i)
  SET_POINT_SCALAR(vtkIntArray, "Pounds", this->m_pounds[IdgFrag_to_IndexFrag[i]])
  SET_POINT_SCALAR(vtkDoubleArray, "Volume", this->m_volume[IdgFrag_to_IndexFrag[i]])

  if (this->DensityArray)
  {
    SET_POINT_SCALAR(vtkDoubleArray, "Mass", this->m_mass[IdgFrag_to_IndexFrag[i]])
    SET_POINT_SCALAR(vtkDoubleArray,
      "Density",
      this->m_mass[IdgFrag_to_IndexFrag[i]] / this->m_volume[IdgFrag_to_IndexFrag[i]])
  }

  if (this->VelocityArray)
  {
    SET_POINT_VECTOR(vtkDoubleArray, "AvgVelocity", this->m_avg_velocity[IdgFrag_to_IndexFrag[i]])
    if (this->DensityArray || this->MassArray)
    {
      SET_POINT_VECTOR(vtkDoubleArray, "Velocity", this->m_velocity[IdgFrag_to_IndexFrag[i]])
    }
  }

  int idBlk = 0;
  output->SetBlock(idBlk, center);
  output->GetMetaData(idBlk)->Set(vtkCompositeDataSet::NAME(), "Centers");

  this->Points.clear();
  this->BlockId.clear();

  return 1;
}
//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::RecursivelyProcessTree(
  vtkHyperTreeGridNonOrientedVonNeumannSuperCursor* cursor)
{
  if (cursor->IsLeaf())
  {
    vtkIdType id = cursor->GetGlobalNodeIndex();

    if (this->InMask && this->InMask->GetValue(id))
    {
      return;
    }

    // Dans materiau
    int fragId = this->FragIds[id];
    if (fragId == -1)
    {
      // Nouveau fragment
      fragId = this->Fragments.size();
      std::list<vtkIdType> lst;
      this->Fragments.push_back(lst);
      this->Fragments[fragId].push_back(id);
      this->FragIds[id] = fragId;
      this->m_pounds.push_back(0);
      this->m_volume.push_back(0);
      this->m_mass.push_back(0);
      double* zeros = new double[3];
      for (int i = 0; i < 3; ++i)
        zeros[i] = 0;
      this->m_avg_velocity.push_back(zeros);
      zeros = new double[3];
      for (int i = 0; i < 3; ++i)
        zeros[i] = 0;
      this->m_velocity.push_back(zeros);
    }

    // this->GhostArray->GetValue(id);

    this->m_pounds[fragId] += 1;

    // La masse
    double* size = cursor->GetSize();
    double cellVolume = size[0] * size[1];
    this->m_volume[fragId] += cellVolume;
    double cellMass = 0;
    if (this->MassArray)
    {
      cellMass = this->MassArray->GetValue(id);
      if (this->DensityArray)
      {
        double cellDensity = this->DensityArray->GetValue(id);
        if (fabs(cellMass - cellVolume * cellDensity) > 1.e-7)
        {
          std::cerr << "Inconsistence mass:" << cellMass << " volume:" << cellVolume
                    << " density:" << cellDensity << std::endl;
        }
      }
      this->m_mass[fragId] += cellMass;
    }
    else if (this->DensityArray)
    {
      cellMass = cellVolume * this->DensityArray->GetValue(id);
      this->m_mass[fragId] += cellMass;
    }
    if (this->VelocityArray)
    {
      double* cellVelocity = this->VelocityArray->GetTuple(id);
      for (int k = 0; k < 3; ++k)
      {
        this->m_avg_velocity[fragId][k] += cellVelocity[k];
      }
      if (cellMass != 0)
      {
        for (int k = 0; k < 3; ++k)
        {
          this->m_velocity[fragId][k] += cellMass * cellVelocity[k];
        }
      }
    }

    for (unsigned int f = 0; f < this->m_dimension; ++f) // dimension
    {
      for (unsigned int o = 0; o < 2; ++o) // gauche, centre, droite
      {
        int neighborIdx = (2 * o - 1) * (f + 1);
        bool isValidN = cursor->HasTree(this->m_dimension + neighborIdx);
        vtkIdType idN = 0;
        if (isValidN)
        {
          idN = cursor->GetGlobalNodeIndex(this->m_dimension + neighborIdx);
        }
        if (isValidN && this->InMask && !this->InMask->GetValue(idN))
        {
          vtkSmartPointer<vtkHyperTreeGridOrientedGeometryCursor> voisin =
            cursor->GetOrientedGeometryCursor(this->m_dimension + neighborIdx);
          if (voisin->IsLeaf())
          {
            // La voisine est presente
            int fragIdN = this->FragIds[idN];
            if (fragIdN == -1)
            {
              // Nouvelle maille dans le fragment
              this->Fragments[fragId].push_back(idN);
              this->FragIds[idN] = fragId;
            }
            else if (fragId != fragIdN)
            {
              // Maille voisine dans un autre fragment : on fusionne
              for (auto it = this->FragIds.begin(); it != this->FragIds.end(); ++it)
              {
                if (*it == fragIdN)
                {
                  *it = fragId;
                }
              }

              for (auto it = this->Fragments[fragIdN].begin(); it != this->Fragments[fragIdN].end();
                   ++it)
              {
                this->Fragments[fragId].push_back(*it);
              }
              this->Fragments[fragIdN].clear();

              this->m_pounds[fragId] += this->m_pounds[fragIdN];
              this->m_pounds[fragIdN] = 0;

              this->m_volume[fragId] += this->m_volume[fragIdN];
              this->m_volume[fragIdN] = 0;

              this->m_mass[fragId] += this->m_mass[fragIdN];
              this->m_mass[fragIdN] = 0;

              for (int d = 0; d < 3; ++d)
              {
                this->m_avg_velocity[fragId][d] += this->m_avg_velocity[fragIdN][d];
                this->m_velocity[fragId][d] += this->m_velocity[fragIdN][d];
              }
              delete[] this->m_avg_velocity[fragIdN];
              this->m_avg_velocity[fragIdN] = nullptr;
              delete[] this->m_velocity[fragIdN];
              this->m_velocity[fragIdN] = nullptr;
            }
          }
        }
        else
        {
          // Mailles de bord
          this->Bords[id] = true;
        }
      }
    }
  }
  else
  {
    // Mere : on parcourt les filles
    for (int child = 0; child < this->m_number_of_children; ++child)
    {
      cursor->ToChild(child);
      this->RecursivelyProcessTree(cursor);
      cursor->ToParent();
    }
  }
}
//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::GenPoints(vtkHyperTreeGridNonOrientedGeometryCursor* cursor)
{
  if (cursor->IsLeaf())
  {
    vtkIdType id = cursor->GetGlobalNodeIndex();

    if (this->InMask && this->InMask->GetValue(id))
    {
      return;
    }

    if (this->ExtractEdge && !this->Bords[id])
    {
      return;
    }

    // Dans materiau
    double pt[3];
    cursor->GetPoint(pt);

    int blockId = this->BlockId[this->FragIds[id]]; // index to idg

    // Insert next point
    vtkIdType outId = this->Points[blockId]->InsertNextPoint(pt);
  }
  else
  {
    // Mere : on parcourt les filles
    for (int child = 0; child < this->m_number_of_children; ++child)
    {
      cursor->ToChild(child);
      this->GenPoints(cursor);
      cursor->ToParent();
    }
  }
}
//----------------------------------------------------------------------------
int vtkHyperTreeGridFragmentation::FillOutputPortInformation(int, vtkInformation* info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkMultiBlockDataSet"); //
  return 1;
}
//----------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
//--------------------------------------------------------------------------------------------------
