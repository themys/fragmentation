This fragmentation filter identifies and characterizes the fragments in a Hyper Tree Grid mesh.

A fragment is described by a grouping of active (unmasked) neighboring leaf cells.

In fact, the fragments are separated by masked leaf cells.

[[_TOC_]]

# Version 0.2
Developed in 2022. Version 0.2 solves the problems of version 0.1 (at least Issues #1 to #4) by putting aside the parallelism aspect.

## Input
With version 0.2, the input has changed a bit.

We always have the option of asking not to output the description of the outline of each of the fragments.

Then, we have the possibility of associating with a semantics a quantity carried by the cells of the mesh.
This quantities are:
- __Mass__: the mass in each cell;
- __Density__: the density in each cell;
- __Velocity__: the velocity in each cell.

A checkbox allows you to activate or not the taking into account of the chosen field of values.

Simply enter the name of a value field that represents mass or density to enable the output of these fields. Similarly, the average speed is output if the name of a speed value field is given.
By associating the name of a field of values ​​which represents a mass or a density with that which represents a speed, one outputs the field of value of speed weighted by the mass.

## Output
The output remains unchanged between version 0.1 and 0.2 except the list of value fields and the way to calculate them.

We find the same value fields:
- for __Centers__ under __CellData__;
- for __Fragment_#__ under __FieldData__. It is just unfortunate that to date ParaView does not allow to colorize a mesh according to a FieldData.

The value fields to a fragment are as follows :
- __FragmentId__: continuous numbering of fragments starting with zero; the naming of the block describing this fragment conforms to this value (unlike version 0.1.0) ;
- __Pounds__: the number of cells that contributes to this fragment;
- __Volume__: the total volume;
- __Mass__: the total mass;
- __Density__: the density, the __Mass__ divided by __Volume__ ;
- __AvgVelocity__: the average velocity, the sum of the velocities of each contributing cell divided by __Pounds__;
- __Velocity__: the velocity as an extensive quantity by the mass, the quotient of the sum of the product of the speed and the mass of each contributing cell divided by __Mass__.

## Fact
A manual test procedure validates this version of the filter.

The sequence performed is as follows:
- load the Armen case with the Hercule reader
  - activate the "HIc API usage" option ; allowing the loading of an AMR mesh under vtkHyperTreeGrid;
  - select only the _Cu1_ material
  - select the __EqPlastique__, __Vitesse__ and __rho__ value fields;

- apply a threshold filter on __EqPlastique__ for the values ​​[0;1.1];

![](etc/fragmentation_1.png "Load and apply threshold filter")

- apply this Fragmentation filter with __rho__ and __Vitesse__ (already checkbox active) with the edges.

![](etc/fragmentation_2.png "Load and apply threshold filter")

You can dynamically explore in a __RenderView__ by selecting the __Hover Points On__ option (a red point surmounted by a question mark) then placing the cursor on a fragment center or one of the nodes participating in the turn of a fragment. The information relating to this fragment will then be visible in a tooltip.

It is possible to isolate the display of a fragment. Be careful, in the __SpreadSheetView__, to select __Attribute__ named __Point Data__ when you explore the __Block Name__ named __Centers__ and to switch to __Attribute__ named __FieldData__ for the blocks describing the perimeter of a fragment.

![](etc/fragmentation_3.png "Load and apply threshold filter")

Like displaying the average velocity field from the centers of the fragments.

![](etc/fragmentation_4.png "Load and apply threshold filter")

## Todo
The main developments are described in Issues #5 to #8.

# Version 0.1
Developed in 2020. The prototype 0.1 did not give satisfaction.

## Input
Optionally, you could ask not to output the description of the contour of each of the fragments.

Indisputably, it was necessary to give the name of two quantities: one which represents the density and the other a speed.

## Output
This filter returns a vtkMultiBlockDataSet:
- the first block __#0__ named __Centers__ is a _vtkPolyData_ of points, each point characterizes the center of a fragment (the average of the coordinates of the cells which make up a fragment) to which we associate fields of values ​​describing this fragment as __CellData__ ;
- the other blocks named __Fragment_#__, with between __#0__ to __#N-1__ (N the number of fragments), are point __vtkPolyData__. Each of these blocks describes the edge of a fragment without privileged order and we associate fields of values ​​describing this fragment as __FieldData__. 

## Fact
It locates the centers of the fragments but with incorrect edges (sometimes cells internal to a fragment were retained). In addition, the associated value fields (CellData or FieldData) were not correctly calculated. The parallel version never worked. And finally, various functionalities were missing, whether at the level of the returned value fields or the characterization of the shape of each fragment.

## Todo
The main failures of this version are described in Issues #1 to #4.
